FROM tigefa/bionic

# Use baseimage-docker's init system.
#CMD ["/sbin/my_init"]

COPY sources.list /etc/apt/sources.list
COPY apt-get.sh /root/apt-get.sh
RUN chmod +x /root/apt-get.sh
#ENV DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true

# Ubuntu desktop
RUN apt-get update -yqq && apt-get dist-upgrade -yqq
RUN apt-get install -yqq apt-utils
RUN apt-get install -yqq sudo python-pip python3-pip apt-transport-https ca-certificates software-properties-common tzdata language-pack-en
RUN locale-gen en_US
RUN update-locale LANG=en_US.UTF-8 LC_CTYPE=en_US.UTF-8
RUN export LC_ALL="en_US.UTF-8" LANG="en_US.UTF-8" LANGUAGE="en_US:en"
RUN locale
RUN apt-get install -yqq ubuntu-server lsb-base lsb-release
RUN apt-get install -yqq ubuntu-desktop
RUN apt-get install -yqq ubuntu-settings
RUN apt-get install -yqq ubuntu-gnome-desktop
RUN apt-get install -yqq ubuntu-gnome-default-settings
RUN apt-get install -yqq vanilla-gnome-desktop
RUN apt-get install -yqq vanilla-gnome-default-settings
RUN apt-get install -yqq xubuntu-desktop
RUN apt-get install -yqq xubuntu-default-settings
RUN apt-get install -yqq chrome-gnome-shell
RUN apt-get install -yqq flashplugin-installer
RUN apt-get install -yqq dirmngr gnupg gnupg2
RUN sudo ln -fs /usr/share/zoneinfo/Asia/Jakarta /etc/localtime
RUN sudo dpkg-reconfigure -f noninteractive tzdata
RUN curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
RUN add-apt-repository ppa:mc3man/mpv-tests -y
RUN add-apt-repository ppa:git-core/ppa -y
RUN add-apt-repository ppa:webupd8team/terminix -y
RUN sudo add-apt-repository ppa:uget-team/ppa -y
RUN sudo add-apt-repository ppa:clipgrab-team/ppa -y
RUN sudo add-apt-repository ppa:peek-developers/stable -y
RUN sudo add-apt-repository ppa:transmissionbt/ppa -y
RUN sudo add-apt-repository ppa:numix/ppa -y
RUN sudo add-apt-repository ppa:numix/numix-daily -y
RUN sudo add-apt-repository ppa:neovim-ppa/stable -y
RUN sudo add-apt-repository ppa:chris-lea/redis-server -y
RUN echo deb [trusted=yes] http://deb.torproject.org/torproject.org bionic main > /etc/apt/sources.list.d/tor.list
RUN echo deb-src [trusted=yes] http://deb.torproject.org/torproject.org bionic main >> /etc/apt/sources.list.d/tor.list
#RUN gpg --keyserver keys.gnupg.net --recv 886DDD89
#RUN gpg --export A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89 | apt-key add -
RUN apt-get update -yqq && apt-get dist-upgrade -yqq
RUN apt-get install -yqq tor deb.torproject.org-keyring
RUN apt-get install -yqq landscape-client landscape-common nodejs wget curl netcat aria2 whois figlet git-all p7zip p7zip-full zip unzip rar unrar
RUN apt-get install -yqq tilix
RUN apt-get install -yqq tightvncserver
RUN apt-get install -yqq gnome-system-monitor
RUN apt-get install -yqq gnome-usage
RUN apt-get install -yqq gnome-terminal
RUN apt-get install -yqq numix*
RUN apt-get install -yqq fonts-ubuntu*
RUN apt-get install -yqq fonts-noto*
RUN apt-get install -yqq clipgrab hexchat glances phantomjs
RUN apt-get install -yqq uget uget-integrator youtube-dl
RUN cd /tmp && aria2c https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb && aria2c https://github.com/oguzhaninan/Stacer/releases/download/v1.0.9/stacer_1.0.9_amd64.deb && dpkg -i *.deb
RUN apt-get -fy install
RUN apt-get update -yqq && apt-get dist-upgrade -yqq
#RUN sudo adduser --disabled-password --gecos 'Developers' developer
RUN apt-get install -yqq google-cloud-sdk #google-chrome-beta google-chrome-unstable google-chrome-stable
#RUN apt-get install -yqq ibm-java80-jdk ibm-java80-jre ibm-java80-plugin
RUN apt-get install -yqq vit tilda tilde terminator terminology termit sakura qterminal picocom moserial microcom guake gtkterm
RUN apt-get install -yqq othman lynx chromium-browser mythbrowser torbrowser-launcher surf surf-display qutebrowser
RUN apt-get install -yqq gedit gedit-plugin-terminal giggle giggle-terminal-view-plugin dconf-editor
RUN apt-get autoremove -y && apt-get autoclean -y
RUN /root/apt-get.sh

# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
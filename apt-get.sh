#! /usr/bin/env bash

apt-get update
apt-get upgrade -y
apt-get dist-upgrade -y
apt-get clean -y
apt-get autoclean -y
apt-get autoremove -y
